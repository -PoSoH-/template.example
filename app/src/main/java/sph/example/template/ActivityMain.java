package sph.example.template;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import sph.example.template.databinding.ActivityMainBinding;
import sph.example.template.ui.ActivityBase;
import sph.example.template.ui.OpenMenu;
import sph.example.template.ui.ViewModelBase;
import sph.example.template.ui.utils.UtilsImage;
import sph.example.template.ui.wallet.FragmentWallet;

public class ActivityMain extends ActivityBase<ActivityMainBinding, ViewModelBase> implements OpenMenu {

    private AppBarConfiguration mAppBarConfiguration;
//    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bindView(R.layout.activity_main);

        ImageView d = dataBinding.navView.getHeaderView(0).findViewById(R.id.imageView);

        UtilsImage.circleImage(d, R.drawable.ic_user_photo);

        DrawerLayout drawer = dataBinding.drawerLayout;
        NavigationView navigationView = dataBinding.navView;
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    @Override
    public void changeMenu() {
        if(mAppBarConfiguration.getOpenableLayout().isOpen()){
            mAppBarConfiguration.getOpenableLayout().close();
        }else{
            mAppBarConfiguration.getOpenableLayout().open();
        }
    }
}