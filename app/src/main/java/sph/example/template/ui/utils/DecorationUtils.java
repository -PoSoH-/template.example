package sph.example.template.ui.utils;

import android.content.Context;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import sph.example.template.R;

public class DecorationUtils {

    public static DividerItemDecoration createDivider(Context context, EOrientation orientation){
        DividerItemDecoration divider = new DividerItemDecoration(
                context,
                orientation == EOrientation.VERTICAL ? LinearLayoutManager.VERTICAL : LinearLayoutManager.HORIZONTAL
        );
        divider.setDrawable(
                EOrientation.VERTICAL == orientation ? context.getDrawable(R.drawable.divider_separator_horizontal) : context.getDrawable(R.drawable.divider_separator_vertical)
        );
        return divider;
    }
}
