package sph.example.template.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Map;

public class ViewModelBase extends ViewModel {

    private final MutableLiveData<Boolean> loading = new MutableLiveData(false);

    private final MutableLiveData<Map<Throwable, String>> error = new MutableLiveData<Map<Throwable, String>>();

    private final MutableLiveData<String> showSnakeMessage = new MutableLiveData();

    public void setShowSnakeMessage(String message){
        showSnakeMessage.setValue(message);
    }
}
