package sph.example.template.ui.utils;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import sph.example.template.R;

public enum ETypeTrade {
    IN (R.string.txt_wallet_trade_type_positive),
    OUT (R.string.txt_wallet_trade_type_negative);

    private int resource;

    ETypeTrade(int resource) {
        this.resource = resource;
    }

    public int getResource(){
        return resource;
    }

    public static ETypeTrade[] getTrades() {
        return values();
    }

    public static ETypeTrade getTrade(int position) {
        return ETypeTrade.values()[position];
    }

    public static List<Integer> getListResource(){
        List<Integer> coll = new ArrayList();
        coll.add(IN.resource);
        coll.add(OUT.resource);
        return coll;
    }
}
