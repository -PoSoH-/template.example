package sph.example.template.ui.models;

import java.util.Date;

import sph.example.template.ui.utils.ETypeTrade;

public class WalletModel {
    private ETypeTrade mTradeType;
    private String mOperationName;
    private float mTradeValue;
    private int mCurrencyType;
    private Date mTradeDate;

    public WalletModel(ETypeTrade mTradeType, String mOperationName, float mTradeValue, int mCurrencyType, Date mTradeDate) {
        this.mTradeType = mTradeType;
        this.mOperationName = mOperationName;
        this.mTradeValue = mTradeValue;
        this.mCurrencyType = mCurrencyType;
        this.mTradeDate = mTradeDate;
    }

    public ETypeTrade getmTradeType() {
        return mTradeType;
    }

    public void setmTradeType(ETypeTrade mTradeType) {
        this.mTradeType = mTradeType;
    }

    public String getmOperationName() {
        return mOperationName;
    }

    public void setmOperationName(String mOperationName) {
        this.mOperationName = mOperationName;
    }

    public float getmTradeValue() {
        return mTradeValue;
    }

    public void setmTradeValue(float mTradeValue) {
        this.mTradeValue = mTradeValue;
    }

    public int getmCurrencyType() {
        return mCurrencyType;
    }

    public void setmCurrencyType(int mCurrencyType) {
        this.mCurrencyType = mCurrencyType;
    }

    public Date getmTradeDate() {
        return mTradeDate;
    }

    public void setmTradeDate(Date mTradeDate) {
        this.mTradeDate = mTradeDate;
    }
}


