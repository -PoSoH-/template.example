package sph.example.template.ui.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

import sph.example.template.databinding.ListItemBoxOfWalletBinding;
import sph.example.template.ui.adapters.holders.HolderWalletData;
import sph.example.template.ui.models.WalletModel;

public class AdapterWalletData extends RecyclerView.Adapter<HolderWalletData> {

    private final List<WalletModel> collections = new LinkedList();

    @SuppressLint("NotifyDataSetChanged")
    public void updateWallets(List<WalletModel> collections){
        if(collections != null && collections.size()>0){
            this.collections.clear();
            this.collections.addAll(collections);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public HolderWalletData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HolderWalletData(ListItemBoxOfWalletBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderWalletData holder, int position) {
        holder.bindItem(collections.get(position));
    }

    @Override
    public int getItemCount() {
        return collections.size();
    }
}
