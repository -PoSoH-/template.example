package sph.example.template.ui.utils;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import sph.example.template.R;

public class UtilsText {

    public static String createPayText(Context context, String value, ETypeTrade type){
        StringBuilder builder = new StringBuilder(type == ETypeTrade.IN ? context.getString(R.string.positive) : context.getString(R.string.negative));
        builder.append(" ");
        builder.append(value);
        builder.append(" ");
        switch (context.getSharedPreferences("SETUP_PARAMS", Context.MODE_PRIVATE).getInt("CURRENCY", 0)){
            case (0) :
                builder.append(context.getString(R.string.currency_uah));
                break;
            case (1):
                builder.append(context.getString(R.string.currency_eur));
                break;
            default:
                builder.append(context.getString(R.string.currency_nil));
        }
        return builder.toString();
    }

    public static String convertDateToTextFill(Date value, Locale local){
        if(local == null) local = Locale.getDefault();
        String outFormat = "dd MMM HH:mm";
        return new SimpleDateFormat(outFormat, local).format(value);
    }

    public static String convertDateToTextMini(Date value, Locale local){
        if(local == null) local = Locale.getDefault();
        String outFormat = "dd MMM";
        return new SimpleDateFormat(outFormat, local).format(value);
    }

    public static String convertDateToTextYear(Date value, Locale local){
        if(local == null) local = Locale.getDefault();
        String outFormat = "dd.MM.YYYY";
        return new SimpleDateFormat(outFormat, local).format(value);
    }
}
