package sph.example.template.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

public class ActivityBase <B extends ViewDataBinding, T extends ViewModelBase> extends AppCompatActivity {

    private ViewModelProvider.Factory provider;

    protected B dataBinding;
    protected T baseViewModel;
    protected ViewModelRoom room;

    protected void bindView(int layoutId) {
        dataBinding = DataBindingUtil.setContentView(this, layoutId);
        provider = getDefaultViewModelProviderFactory();
    }

    @Override
    protected void onDestroy() {
//        baseViewModel.detachView();
        super.onDestroy();
    }

    public ViewModelProvider.Factory getProvider() {
        return provider;
    }

    public ViewModelRoom getRoom(){
        if(room == null){
            room = new ViewModelProvider(this).get(ViewModelRoom.class);
            room.initRoom(getApplication());
        }
        return room;
    }

    public void showSnack(int resourceString){
        Snackbar.make(
                    dataBinding.getRoot(),
                    getString(resourceString),
                    Snackbar.LENGTH_LONG)
                .setActionTextColor(ContextCompat.getColor(this, android.R.color.holo_red_light))
                .show();
    }

}
