package sph.example.template.ui.home;

import static android.content.Context.LOCATION_SERVICE;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import sph.example.template.ActivityMain;
import sph.example.template.R;
import sph.example.template.databinding.FragmentMapBinding;
import sph.example.template.ui.FragmentBase;
import sph.example.template.ui.OpenMenu;

public class FragmentMap
        extends FragmentBase<FragmentMapBinding>
        implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private LocationManager mLocation;
    private MarkerOptions mMarker;

    private double mLat = 0.0d;
    private double mLon = 0.0d;
    private boolean positionUpdated = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MapViewModel mapViewModel =
                new ViewModelProvider(this).get(MapViewModel.class);

        mMarker = new MarkerOptions()
                .position(new LatLng(mLat, mLon))
                .title(getString(R.string.my_state));

        bindView(R.layout.fragment_map, inflater, container);
        View root = dataBinding.getRoot();

        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getBaseActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.for_map, mapFragment)
                .commit();
        mapFragment.getMapAsync(this);

        dataBinding.centeredMapAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mMarker.position(new LatLng(mLat, mLon));
                    updateMap();
                }
            }
        );

        dataBinding.showDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((ActivityMain)requireActivity()) instanceof OpenMenu)
                    ((ActivityMain)requireActivity()).changeMenu();
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        mLocation = (LocationManager) getBaseActivity().getSystemService(LOCATION_SERVICE);
        chekPermissions();
        super.onResume();
    }

    @Override
    public void onPause() {
        mLocation.removeUpdates(this);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dataBinding = null;
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng first = new LatLng(mLat, mLon);

        mMap.addMarker(new MarkerOptions()
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
                .position(first).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(first));
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        || grantResults[1] == PackageManager.PERMISSION_GRANTED){
                    startManagerNetwork();
                    startManagerGPS();
                } else{
//                    Toast.makeText(this, "Not enabled manager location", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void chekPermissions(){
        if (getBaseActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && getBaseActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            startManagerGPS();
            startManagerNetwork();
        }else{
            getBaseActivity().requestPermissions(
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    }, PackageManager.PERMISSION_GRANTED);
        }
    }

    private void startManagerGPS() {
        if (getBaseActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocation.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 1000 * 10, 10, this);
        }
    }

    private void startManagerNetwork() {
        if (getBaseActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocation.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 1000 * 10, 10, this);
        }
    }

    private void updateMap(){
        mMap.addMarker(mMarker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mLat, mLon))
                .zoom(18)
                .bearing(0)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        positionUpdated = true;
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        mLat = location.getLatitude();
        mLon = location.getLongitude();
        if(!positionUpdated && mMap != null) {
            mMarker.position(new LatLng(mLat, mLon));
            updateMap();
        }
    }

    @Override
    public void onLocationChanged(@NonNull List<Location> locations) {
        LocationListener.super.onLocationChanged(locations);
    }

    @Override
    public void onFlushComplete(int requestCode) {
        LocationListener.super.onFlushComplete(requestCode);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        LocationListener.super.onStatusChanged(provider, status, extras);
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
        LocationListener.super.onProviderEnabled(provider);
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        if(provider.toLowerCase().equals("gps".toLowerCase())){
            getBaseActivity().showSnack(R.string.error_enable_gps);
        }else{
            LocationListener.super.onProviderDisabled(provider);
        }
    }
}