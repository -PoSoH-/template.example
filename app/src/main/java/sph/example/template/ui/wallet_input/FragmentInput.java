package sph.example.template.ui.wallet_input;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import java.util.Date;

import sph.example.template.R;
import sph.example.template.databinding.FragmentInputBinding;
import sph.example.template.ui.ActivityBase;
import sph.example.template.ui.FragmentBase;
import sph.example.template.ui.adapters.AdapterTypeTrade;
import sph.example.template.ui.utils.UtilsText;

public class FragmentInput extends FragmentBase<FragmentInputBinding> {

    private final String TAG = "FragmentInput";

    private ViewModelInput inputViewModel;
    private AdapterTypeTrade typeTradeAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        inputViewModel = new ViewModelProvider(this).get(ViewModelInput.class);

        typeTradeAdapter = new AdapterTypeTrade();

        bindView(R.layout.fragment_input, inflater, container);
        View root = dataBinding.getRoot();

        dataBinding.toolbarLabel.setText(R.string.input_trade);
        getBaseActivity().setSupportActionBar(dataBinding.toolbar);

        Spinner spinner = ((Spinner)root.findViewById(R.id.action_bar_type_select));
        spinner.setAdapter(typeTradeAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                inputViewModel.setSelectedPosition(typeTradeAdapter.getSelectedPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        dataBinding.setVmInput(inputViewModel);
        dataBinding.executePendingBindings();

        dataBinding.backTransit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigateUp();
            }
        });

        inputViewModel.actionDate().observe(getViewLifecycleOwner(), this::showDate);
        inputViewModel.stateCalendar().observe(getViewLifecycleOwner(), this::showCalendar);
        inputViewModel.stateBack().observe(getViewLifecycleOwner(), this::stateBack);

        ActivityBase d = getBaseActivity();
        inputViewModel.setRoomViewModel(getBaseActivity().getRoom());

        return root;
    }

    private void showCalendar(Boolean state){
        if(state!=null && state){
            Log.e(TAG, "SHOW");
        }
    }

    private void stateBack(Boolean state){
        if(state!=null && state){
            getNavController().navigateUp();
        }
    }

    private void showDate(Date data){
        if(data!=null){
            ((TextView)dataBinding.getRoot().findViewById(R.id.intent_input_data_value_field))
                    .setText(UtilsText.convertDateToTextYear(data, null));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dataBinding = null;
    }
}