package sph.example.template.ui.wallet_input;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Date;
import java.util.List;

import sph.example.template.ui.ViewModelRoom;
import sph.example.template.ui.models.WalletModel;
import sph.example.template.ui.utils.ETypeTrade;

public class ViewModelInput extends ViewModel {

    private final String TAG = "ViewModelInput";
    private ViewModelRoom room;

    private final MutableLiveData<List<Integer>> mTypeTrade;
    private final MutableLiveData<Boolean> mShowCalendar = new MutableLiveData(false);
    private final MutableLiveData<Date> mDateCreate;
    private final MutableLiveData<Boolean> mStateBack = new MutableLiveData(false);

    private String name;
    private float value;
    private ETypeTrade baseType = ETypeTrade.IN;
    private int mCurrentCurrency = 0;

    public ViewModelInput() {
        mTypeTrade = new MutableLiveData<>();
        mTypeTrade.setValue(ETypeTrade.getListResource());
        mDateCreate = new MutableLiveData(new Date(System.currentTimeMillis()));
    }

    public LiveData<List<Integer>> getSpinnerList() {
        return mTypeTrade;
    }
    public LiveData<Boolean> stateCalendar() { return mShowCalendar; }
    public LiveData<Date> actionDate() { return mDateCreate; }
    public LiveData<Boolean> stateBack() { return mStateBack; }

    public void setSelectedPosition(ETypeTrade newType){
        baseType = newType;
    }

    public void showCalendar(){
        mShowCalendar.setValue(!mShowCalendar.getValue());
    }

    public void onTextIntent(CharSequence text, int start, int before, int count){
        Log.e(TAG, text.toString());
        if(text!=null && text.length() > 0) name = text.toString();
    }

    public void onTextValue(CharSequence text, int start, int before, int count){
        Log.e(TAG, text.toString());
        if(text!=null && text.length() > 0) value = Float.parseFloat(text.toString());
    }

    public void createPosition(){
        this.room.insertTrade(new WalletModel(
                baseType,
                name,
                value,
                mCurrentCurrency,
                mDateCreate.getValue())
        );
        mStateBack.setValue(true);
    }

    public void setRoomViewModel(ViewModelRoom room){
        this.room = room;
    }
}