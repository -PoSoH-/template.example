package sph.example.template.ui;

import android.app.Application;

import androidx.lifecycle.ViewModel;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import sph.example.data.db.models.TradeEntity;
import sph.example.data.db.repository.RepositoryTrade;
import sph.example.template.ui.models.WalletModel;
import sph.example.template.ui.utils.ETypeTrade;

public class ViewModelRoom extends ViewModel {

    private final String TAG = "ViewModelRoom";

    private RepositoryTrade mRoomRepository;

    public ViewModelRoom() {
    }

    public void initRoom(Application application) {
        mRoomRepository = new RepositoryTrade(application);
    }

    public List<WalletModel> getAllTrades() {
        List<WalletModel> model = new LinkedList<>();
        List<TradeEntity> trades = mRoomRepository.getAllTrades();
        if (trades == null) return model;
        else {
            model.clear();
            for (TradeEntity tradeEntity : trades) {
                model.add(new WalletModel(
                        tradeEntity.getTradeType().equals(ETypeTrade.IN.toString()) ? ETypeTrade.IN : ETypeTrade.OUT,
                        tradeEntity.getOperationName(),
                        (float) tradeEntity.getTradeValue(),
                        tradeEntity.getCurrencyType(),
                        convert(tradeEntity.getTradeDateMillis())));
            }
            return model;
        }
    }

    private Date convert(long time) {
        Date date = new Date();
        date.setTime(time);
        return date;
    }

    public void insertTrade(WalletModel model) {
        TradeEntity trade = new TradeEntity();
        trade.setTradeType(model.getmTradeType().toString());
        trade.setCurrencyType(model.getmCurrencyType());
        trade.setTradeValue(model.getmTradeValue());
        trade.setTradeDateMillis(model.getmTradeDate().getTime());
        trade.setOperationName(model.getmOperationName());
        mRoomRepository.insert(trade);
    }

}