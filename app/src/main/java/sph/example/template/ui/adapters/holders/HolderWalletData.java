package sph.example.template.ui.adapters.holders;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.util.TypedValue;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import sph.example.template.R;
import sph.example.template.databinding.ListItemBoxOfWalletBinding;
import sph.example.template.ui.models.WalletModel;
import sph.example.template.ui.utils.ETypeTrade;
import sph.example.template.ui.utils.UtilsText;

public class HolderWalletData extends RecyclerView.ViewHolder {

    private ListItemBoxOfWalletBinding view;

    public HolderWalletData(@NonNull ListItemBoxOfWalletBinding itemView) {
        super(itemView.getRoot());
        view = itemView;
    }

    public void bindItem(WalletModel model){
        view.txtItemBoxTradeName.setText(model.getmOperationName());
        int[][] states = new int[][] {new int[] { android.R.attr.state_enabled}};
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = view.getRoot().getContext().getTheme();
        theme.resolveAttribute(R.attr.image_tint, typedValue, true);
        if(model.getmTradeType()==ETypeTrade.IN){
            view.txtItemBoxTradeValue.setTextColor(
                    ContextCompat.getColor(view.getRoot().getContext(),R.color.green_cian)
            );
            view.imgItemBoxTradeType.setImageResource(R.drawable.ic_credit_card);
            view.imgItemBoxTradeType.setImageTintList(new ColorStateList(states, new int[]{typedValue.data} ));
            view.txtItemBoxTradeValue.setText(UtilsText.createPayText(view.getRoot().getContext(), String.valueOf(model.getmTradeValue()), ETypeTrade.IN));
            view.txtItemBoxTradeDateVal.setText(UtilsText.convertDateToTextFill(model.getmTradeDate(), null));
        }else{
            view.txtItemBoxTradeValue.setTextColor(
                    ContextCompat.getColor(view.getRoot().getContext(),R.color.red_base)
            );
            view.imgItemBoxTradeType.setImageResource(R.drawable.ic_person);
            view.imgItemBoxTradeType.setImageTintList(new ColorStateList(states, new int[]{typedValue.data} ));
            view.txtItemBoxTradeValue.setText(UtilsText.createPayText(view.getRoot().getContext(), String.valueOf(model.getmTradeValue()), ETypeTrade.OUT));
            view.txtItemBoxTradeDateVal.setText(UtilsText.convertDateToTextMini(model.getmTradeDate(), null));
        }
    }
}
