package sph.example.template.ui.utils;

import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class UtilsImage {

    public static void circleImage(ImageView view, int res){
        Glide.with(view.getContext())
                .load(res)
                .circleCrop()
                .into(view);
    }

}
