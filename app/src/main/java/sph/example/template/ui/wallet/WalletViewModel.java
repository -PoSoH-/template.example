package sph.example.template.ui.wallet;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.LinkedList;
import java.util.List;

import sph.example.template.ui.ViewModelBase;
import sph.example.template.ui.ViewModelRoom;
import sph.example.template.ui.models.WalletModel;

public class WalletViewModel extends ViewModelBase {

    private ViewModelRoom room;
    private final MutableLiveData<List<WalletModel>> mWallet;

    public WalletViewModel() {
        this.mWallet = new MutableLiveData(new LinkedList<WalletModel>());
    }

    public LiveData<List<WalletModel>> getWallet(){
        return mWallet;
    }

    public void updateList(){
        List<WalletModel> temp = this.room.getAllTrades();
        mWallet.setValue(this.room.getAllTrades());
    }

    public void setRoomViewModel(ViewModelRoom room){
        this.room = room;
    }
}