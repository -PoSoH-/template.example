package sph.example.template.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.appcompat.widget.AppCompatTextView;

import sph.example.template.R;
import sph.example.template.ui.utils.ETypeTrade;

public class AdapterTypeTrade extends BaseAdapter {

    private final ETypeTrade[] collections = ETypeTrade.getTrades();

    @Override
    public int getCount() {
        return collections.length;
    }

    @Override
    public ETypeTrade getItem(int i) {
        return ETypeTrade.getTrade(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_item_show, null);
        AppCompatTextView names = (AppCompatTextView) view.findViewById(R.id.spinner_position);
        names.setText(view.getContext().getString(collections[i].getResource()));
        return view;
    }

    public ETypeTrade getSelectedPosition(int position){
        return collections[position];
    }
}
