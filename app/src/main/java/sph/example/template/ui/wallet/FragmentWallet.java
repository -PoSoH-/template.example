package sph.example.template.ui.wallet;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Objects;

import sph.example.template.R;
import sph.example.template.databinding.FragmentWalletBinding;
import sph.example.template.ui.FragmentBase;
import sph.example.template.ui.ViewModelRoom;
import sph.example.template.ui.adapters.AdapterWalletData;
import sph.example.template.ui.utils.DecorationUtils;
import sph.example.template.ui.utils.EOrientation;

public class FragmentWallet extends FragmentBase<FragmentWalletBinding> {

    private WalletViewModel walletViewModel;
    private AdapterWalletData adapterWalletData;

    private RecyclerView walletList;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        bindView(R.layout.fragment_wallet, inflater, container);
        View root = dataBinding.getRoot();

        dataBinding.toolbarLabel.setText(R.string.title_wallet);
        getBaseActivity().setSupportActionBar(dataBinding.toolbar);

        adapterWalletData = new AdapterWalletData();
        walletList = (RecyclerView)(dataBinding.lstWalletTradings);
        walletList.addItemDecoration(DecorationUtils.createDivider(getContext(), EOrientation.VERTICAL));
        walletList.setAdapter(adapterWalletData);

        dataBinding.btnWalletTradeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("TAG", "Add button press....");
                navController.navigate(R.id.nav_input_data);
            }
        });

        dataBinding.backTransit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigateUp();
            }
        });

        walletViewModel = new ViewModelProvider(this).get(WalletViewModel.class);
        walletViewModel.getWallet().observe(getViewLifecycleOwner(), adapterWalletData::updateWallets);
        walletViewModel.setRoomViewModel(getBaseActivity().getRoom());
        walletViewModel.updateList();
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dataBinding = null;
    }

}