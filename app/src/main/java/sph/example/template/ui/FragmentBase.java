package sph.example.template.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

public class FragmentBase<B extends ViewDataBinding> extends Fragment {

    protected NavController navController;
    private ActivityBase baseActivity;
    protected B dataBinding;
    private int layoutId;
//    protected T baseViewModel;

    protected void bindView(int layoutId, LayoutInflater inflater, ViewGroup container) {
        this.layoutId = layoutId;
        baseActivity = (ActivityBase) getActivity();
        dataBinding = DataBindingUtil.bind(inflater.inflate(layoutId, container, false), null);
        navController = NavHostFragment.findNavController(this);
    }

//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
////        dataBinding = inflater.inflate(layoutId, container, false);
//
////        return dataBinding.getRoot();
//    }

//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//
//
//        super.onViewCreated(view, savedInstanceState);
//    }

    public ActivityBase getBaseActivity(){
        return baseActivity;
    }

    public NavController getNavController() {
        return navController;
    }
}
