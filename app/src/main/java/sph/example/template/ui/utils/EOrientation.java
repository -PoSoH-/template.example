package sph.example.template.ui.utils;

public enum EOrientation {
    VERTICAL,
    HORIZONTAL
}
