package sph.example.data.db.contracts;

public class TradeContract {

    public static final String fetchAllTrading = "SELECT * FROM " + ContractTableRoom.tableTrade;
    public static final String fetchWithId = "SELECT * FROM " + ContractTableRoom.tableTrade + " WHERE id = :tId";
    public static final String insertTrade = "INSERT * FROM " + ContractTableRoom.tableTrade;
    public static final String deleteWithId = "DELETE FROM " + ContractTableRoom.tableTrade + " WHERE Id = :tId ";

}
