package sph.example.data.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import sph.example.data.db.models.TradeEntity;
import sph.example.data.db.contracts.TradeContract;

@Dao
public interface ITradeDao {
    @Query(TradeContract.fetchAllTrading)
    List<TradeEntity> fetchAllTrades();

    @Query(TradeContract.fetchWithId)
    TradeEntity fetchTradeById(long tId);

    @Insert(entity = TradeEntity.class, onConflict = OnConflictStrategy.REPLACE)
    void insertTrade(TradeEntity trade);

    @Delete(entity = TradeEntity.class)
    void deleteTrades(TradeEntity cardEntity);

}
