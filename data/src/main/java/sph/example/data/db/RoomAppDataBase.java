package sph.example.data.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import sph.example.data.db.contracts.ContractTableRoom;
import sph.example.data.db.converters.ConverterDate;
import sph.example.data.db.dao.ITradeDao;
import sph.example.data.db.models.TradeEntity;


@Database(entities = {TradeEntity.class}, version = 1)
public abstract class RoomAppDataBase extends RoomDatabase {

    public abstract ITradeDao tradeDao();

    private static volatile RoomAppDataBase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static RoomAppDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomAppDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomAppDataBase.class, ContractTableRoom.database)
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            databaseWriteExecutor.execute(() -> {
                ITradeDao dao = INSTANCE.tradeDao();
            });
        }
    };

}
