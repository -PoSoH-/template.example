package com.raduga.data.repository

import com.raduga.data.Constants
import com.raduga.data.entity.*
import io.reactivex.Single
import retrofit2.http.*

interface CartApi {

    @GET("api/v1/crm/cart")
    fun getCart(
        @Query("cartUid") uid: String? = null,
        @Query("location_id") location_id: Int? = Constants.defaultCity.locationId,
        @Query("promocode") promocode: String? = null,
                ): Single<DataEntity<CartDataEntity>>

    @POST("api/v1/crm/cart/add")
    fun addProduct(
        @Query("productId") id: Int,
        @Query("positionCount") positionCount: Int? = null,
        @Query("addPositionCount") addPositionCount: Int? = null,
        @Query("cartUid") cartUid: String? = null,
        @Query("location_id") location_id: Int? = Constants.defaultCity.locationId,
        @Query("promocode") promocode: String? = null
    ): Single<DataEntity<CartDataEntity>>

    @POST("api/v1/crm/cart/remove")
    fun deleteProductFromCart(
        @Query("cartUid") cartUid: String? = null,
        @Query("productId") productId : Int,
        @Query("location_id") location_id: Int? = Constants.defaultCity.locationId,
        @Query("promocode") promocode: String? = null
    ): Single<DataEntity<CartDataEntity>>

    @POST("/api/v1/crm/checkout/verify-promocode")
    fun verifyPromocode(
        @Query("promocode") cartUid: String? = null
    ): Single<DataEntity<SuccessEntity>>
}