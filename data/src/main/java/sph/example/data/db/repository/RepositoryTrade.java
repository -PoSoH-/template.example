package sph.example.data.db.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import sph.example.data.db.RoomAppDataBase;
import sph.example.data.db.dao.ITradeDao;
import sph.example.data.db.models.TradeEntity;

public class RepositoryTrade {
    private ITradeDao mTrade;
    private List<TradeEntity> mAllTrades;

    public RepositoryTrade(Application application){
        RoomAppDataBase db = RoomAppDataBase.getDatabase(application);
        mTrade = db.tradeDao();
        RoomAppDataBase.databaseWriteExecutor.execute(() -> {
            mAllTrades = mTrade.fetchAllTrades();
        });
    }

    public List<TradeEntity> getAllTrades(){
        return mAllTrades;
    }

    public void insert(TradeEntity data){
        RoomAppDataBase.databaseWriteExecutor.execute(() -> {
            mTrade.insertTrade(data);
        });
    }

}
