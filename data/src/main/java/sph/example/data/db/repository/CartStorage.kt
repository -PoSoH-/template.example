package com.raduga.data.repository

import android.content.SharedPreferences
import com.google.gson.Gson
import com.raduga.data.entity.CartEntity
import com.raduga.data.extension.inAsyncTransaction
import com.raduga.data.mapper.CartDataEntityMapper
import com.raduga.domain.interactor.GetCartUseCase
import com.raduga.domain.model.CartData
import com.raduga.domain.repository.CartRepository
import com.raduga.domain.repository.UserPreferencesRepository
import io.reactivex.Single
import java.lang.Exception
import javax.inject.Inject

class CartStorage @Inject constructor(
    private val cartApi: CartApi,
    private val userPreferencesRepository: UserPreferencesRepository,
    private val preferences: SharedPreferences,
    private val gson: Gson
) : CartRepository {

    companion object {
        const val CART_INFO = "cart_info"
    }

    override fun getCartInfo(params: GetCartUseCase.Params): Single<CartData> {
        val cachedCart = getCartInfoCacheInternal()
        return cartApi.getCart(
            uid = cachedCart?.uid,
            location_id = userPreferencesRepository.getSelectedCity().locationId,
            promocode = params.promocode
        ).map {
            val cart = it.data.cart
            cart.productsCount = it.data.positions.size
            saveCartInfoCache(cart)
            it.data
        }.map(
            CartDataEntityMapper::transformToDomain
        )
    }

    override fun addProductToCart(params: GetCartUseCase.Params): Single<CartData> {
        val cachedCart = getCartInfoCacheInternal()
        return cartApi.addProduct(
            id = params.id ?: 0,
            cartUid = params.cartUid ?: cachedCart?.uid,
            positionCount = params.positionCount,
            addPositionCount = params.addPositionCount,
            location_id = userPreferencesRepository.getSelectedCity().locationId,
            promocode = params.promocode ?: cachedCart?.promocode
        ).map {
            val cart = it.data.cart
            cart.productsCount = it.data.positions.size
            saveCartInfoCache(cart)
            it.data
        }.map(
            CartDataEntityMapper::transformToDomain
        )
    }

    override fun removeProductFromCart(params: GetCartUseCase.Params): Single<CartData> {
        val cachedCart = getCartInfoCacheInternal()
        return cartApi.deleteProductFromCart(
            cartUid = params.cartUid ?: cachedCart?.uid,
            productId = params.id ?: 0,
            location_id = userPreferencesRepository.getSelectedCity().locationId,
            promocode = params.promocode
        ).map {
            val cart = it.data.cart
            cart.productsCount = it.data.positions.size
            saveCartInfoCache(cart)
            it.data
        }.map (
            CartDataEntityMapper::transformToDomain
        )
    }

    override fun verifyPromocode(params: String): Single<Boolean> {
        return cartApi.verifyPromocode(params).map {
            it.data.success
        }
    }

    fun saveCartInfoCache(cart: CartEntity) {
        preferences.inAsyncTransaction {
            putString(CART_INFO, gson.toJson(cart, CartEntity::class.java))
        }
    }

    fun getCartInfoCacheInternal(): CartEntity? {
        var cart: CartEntity? = null
        try {
            cart = gson.fromJson(
                preferences.getString(CART_INFO, ""),
                CartEntity::class.java
            )
        } catch (e: Exception) {

        }
        return cart
    }
}