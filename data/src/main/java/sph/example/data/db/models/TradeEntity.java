package sph.example.data.db.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

import sph.example.data.db.contracts.ContractTableRoom;

@Entity(tableName = ContractTableRoom.tableTrade)
public class TradeEntity {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long id;
    private String mTradeType;
    private String mOperationName;
    private double mTradeValue;
    private int mCurrencyType;
    private Long mTradeDateMillis;

    public TradeEntity(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTradeType() {
        return mTradeType;
    }

    public void setTradeType(String mTradeType) {
        this.mTradeType = mTradeType;
    }

    public String getOperationName() {
        return mOperationName;
    }

    public void setOperationName(String mOperationName) {
        this.mOperationName = mOperationName;
    }

    public double getTradeValue() {
        return mTradeValue;
    }

    public void setTradeValue(float mTradeValue) {
        this.mTradeValue = mTradeValue;
    }

    public int getCurrencyType() {
        return mCurrencyType;
    }

    public void setCurrencyType(int mCurrencyType) {
        this.mCurrencyType = mCurrencyType;
    }

    public Long getTradeDateMillis() {
        return mTradeDateMillis;
    }

    public void setTradeDateMillis(Long mTradeDate) {
        this.mTradeDateMillis = mTradeDate;
    }
}
